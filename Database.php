<?php

namespace Classes;
use \PDO;

class Database
{

  public $pdo; // Acesso público da conexão

  /* Faz conexão com o Banco de Dados */
  public function conn($dbhost, $dbtype, $dbname, $dbuser, $dbpass)
  {
    try {
      $this->pdo = new PDO($dbtype.':dbname='.$dbname.';host='.$dbhost, $dbuser, $dbpass);
    } catch(PDOException $e) {
      echo $e->getMessage();
      exit();
    }
  }


  /* Puxa dados do Banco */
  public function select($columns = [], $table, $condition = null)
  {
    // SELECT name, email FROM users WHERE id = 1

    $query = 'SELECT '.implode(', ', $columns).' FROM '.$table;

    if(!is_null($condition)) {
      $query .= ' WHERE '.$condition;
    }

    $sql = $this->pdo->prepare($query);
    $sql->execute();

    return $sql->fetchAll();

  }

  /* Insere dados no Banco */
  public function insert($table, $data)
  {
    // INSERT INTO users SET email = anderson@gmail.com, password = '123'

    $data_format = [];

    foreach($data as $column => $value) {
      $line = $column.' = "'.$value.'"';

      array_push($data_format, $line);
    }

    $data = implode(', ', $data_format);

    $query = 'INSERT INTO '.$table.' SET '.$data;

    $sql = $this->pdo->prepare($query);
    $sql->execute();

    return $this->pdo->lastInsertId();

  }

  /* Deleta dados do Banco */
  public function delete($columns = ['*'], $table, $condition = null)
  {
    // DELETE email, password FROM users WHERE id = 1

    if($columns[0] == '*') {
      $columns = [];
    }

    $query = 'DELETE '.implode(', ', $columns).' FROM '.$table.(!is_null($condition) ? ' WHERE '.$condition : '');

    $sql = $this->pdo->prepare($query);
    $sql->execute();

    return $sql->rowCount();
  }


  /* Atualiza dados do Banco */
  public function update($table, $data, $condition = null)
  {
    // UPDATE FROM users SET anderson = 'Anderson F. da Silva' WHERE id = 1

    $data_format = [];

    foreach($data as $key => $value) {
      $line = $key.' = "'.$value.'"';

      array_push($data_format, $line);
    }

    $query = 'UPDATE '.$table.' SET '.implode(', ', $data_format).(!is_null($condition) ? ' WHERE '.$condition : '');

    $sql = $this->pdo->prepare($query);
    $sql->execute();

    return $sql->rowCount();

  }

}

?>
